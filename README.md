# Test

Welcome to Obsidian, we have a Customer that wants you to configure an Apache Webserver for them,
they have an internal policy that says that their System must use Containers.
- Clone this repo to your server.
- Login into your Server
	- Create a new user called **"`frikkie`"** with a Password called **"`Frikkie1024`"**
	- Create a group called **"`containers`"** and add **"`frikkie`"** to it
	- The **"`frikkie`"** user must be able to become "**`root`**" via **`sudo`**
	- Any User that belongs to the **"`containers`"** group should be able to become **"`root`"**
	- Install the "**`nmap`**" package

- The Server has an External Disk provisioned to it
	- Please Configure this Disk as a **Logical Volume of 5Gig**
	- The Volume must be resizable while mounted (Please think about your Filesystem type)
	- Please ensure that this Disk is persisted Mounted on **"`/data`"**

- Application
	- Please use any Container technology to use this image  "**`httpd:2.4-alpine`**" available on Docker Hub
	- The Container name should be **"`httpd`"**
    - Copy the Contents of **`http_root`** into **"`/data`"**
	- This Container should use **`"/data"`** as the default Document Root
	- The Exposed Port that Apache should be running on Port **`8000`**
	- The Apache Container **should be running** after a restart
    - Test this `http://<SERVER_IP_ADDR>:8000`

- Security
	- Please ensure that the Firewall is running on this Server
	- Please ensure that Port **`8000`** is open
	- Please ensure that Port **`22`** is open

- Automation
	- Write a Script or Automation to create the following 3 users with Passwords
		- Username: **"`glenville`"** Password: **"`Glenville1024`"**
		- Username: **"`robert`"** Password: **"`Robert1024`"**
		- Username: **"`yamkela`"** Password: **"`Yamkela1024`"**
	- The users must all be in the **"`containers`"** group
	- These users should exist on the server

- Bonus Points
	- Ensure the Platform's Security Framework is running and configured (**`SELinux/AppArmor`**)

# Notes about the Test
- you **can** Google, you can use any knowledge you have, but you have 1 hour for the test
- you **can** ask for help
- At the end of the test, **you will present** your work to our Technical Team
	- Where we will ask you various questions.

